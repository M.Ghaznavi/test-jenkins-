*** Settings ***
Resource    ../keywords.robot
Suite Setup      Wait Until Keyword Succeeds   ${RETRY_INT_LOGIN}  ${SHORT_DELAY}  Open Alex
Suite Teardown   Close Application
Force Tags      Capture

*** Test Cases ***
Download Import Manager Files
    [Documentation]  Users are able to download an import manager file from a job
    [Tags]  Download
    Click Capture Tab
    Download Import-Manager Files From The First Job
    [Teardown]   Go To Home Page

Verify Dataset Upload
    [Documentation]  Alex is able to import new dataset, check and rollback the dataset
    [Tags]  Import   Fatal
    Click Capture Tab
    @{LIST_OF_FILES} =     List Files In Directory     ${IMPORT_TEST_DIR}
    Import Data Assets To Alex      ${LIST_OF_FILES}
    Verify Dataset Upload
    [Teardown]   Go To Home Page


#Import Dataset Files
#    [Documentation]  Alex is able to import new dataset
#     Click Capture Tab
#     @{LIST_OF_FILES} =     List Files In Directory     ${IMPORT_DIR}
#     Import Data Assets     ${LIST_OF_FILES}
#     [Teardown]   Go To Home Page




