*** Settings ***
Resource    ../keywords.robot
Suite Setup      Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}  Open Application
Suite Teardown   Close Application
Force Tags  Login

*** Test Cases ***
Open Login Page
    [Documentation]  Users are directed to login page
    [Tags]     Fatal
    Set Selenium Speed    0.5 seconds
    Login Page Should Be Open

Invalid Login
    [Documentation]  This is an Invalid Login scenario
    [Tags]      InvalidLogin  Fatal
    Continue
    Wait Until Page Contains Element    //*[@id="loginForm"]/div[2]//*[text()='Please enter your username']
    ${currentUrl}    Get Location
    Should Be Equal     ${currentUrl}   ${URL}/login?redirect=

Valid Login
    [Documentation]  This is a Valid Login scenario
    [Tags]      ValidLogin    Fatal
    Set Selenium Speed    0.2 seconds
    Input Username    ${USERNAME}
    Continue
    Input Password    ${PASSWORD}
    Submit Credentials
    Welcome Page Should Be Open
    [Teardown]   Go To Home Page


Log Out And Log In Again
    [Documentation]  This is a logout and login scenario
    [Tags]      Logout    ValidLogin
    Sleep   20s
    Log Out
    Input Username    ${USERNAME}
    Continue
    Input Password    ${PASSWORD}
    Submit Credentials
    Welcome Page Should Be Open
    [Teardown]   Go To Home Page



