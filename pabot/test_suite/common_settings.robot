*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem
Library  DateTime
Library  String
Library  Collections
Library  customized_keywords/MultipleFilesUpload.py
Library  customized_keywords/HeadlessFileDownload.py


*** Variables ***
#${URL}          https://release-test.alex4im.com
#${URL}           https://release247-test.alex4im.com
${URL}      http://54.206.124.239
${BROWSER}  headlesschrome
${USERNAME}     admin
${PASSWORD}     Alexatlas123
${DELAY}        10seconds
${SHORT_DELAY}  5seconds
${RETRY_INT_LOGIN}  3 minutes
${RETRY_INT_OP}     1 minutes
${ALEX_HOME}    ${URL}/home
${ALEX_LOGOUT}    ${URL}/logout
${BUS_NAME_SEARCH_TERM}    Table 1180353
${TECHNOLOGY}       Technology
${TECHNOLOGY_COL_NO}    12
${TECHNOLOGY_FILTER}    Oracle
${TYPE_OF_ASSET}    Type of Asset
${DATA_FLOW}    Data Flow
${DATA_FLOW_COL_NO}    13
${ASSET_COL_NO}     5
${ASSET_FILTER}     Table
${ASSET_FILTER_2}   Column
${DOWNLOAD_LOCATION}    /Users/neepachakraborty/Downloads
${IMPORT_DIR}     /Users/neepachakraborty/Downloads/ReleaseSet/
${IMPORT_TEST_DIR}     /Users/neepachakraborty/Downloads/test_data_upload/
${PROPERTY_SEARCH_TERM}  table
${BUS_NAME_SUBSTR}  Process
${ASSET_PROPERTY}  Business name
${ASSET_PROPERTY_VALUE}  Customer
${NEIGHBOUR_FOR_NODE}   Table 1180353
${NUM_OF_PAGES_ORACLE}    127
${CI_SEARCH_TERM}       func
${REL_TYPE_VAL}     Cust
${PART_OF}      PART_OF
${SQL_SERVER}   SQL Server
${SELENIUM_SPEED}       1s
${ENRICH_TEST_DATA}     Data Process 163709
${PACKAGE}      Function
${IMPORT_DELAY}     40s
${EXPLORE_SEARCH_NODE}     Table 2024375

# User Admin
${USER_FIRST_NAME}  dummy
${USER_LAST_NAME}   dummy
${USER_PASSWORD}    Qwerty1234
${USER_EMAIL}       dummy@alexsolutions.com.au
${USER_CONTACT}     0412346354
${ROLE_DESCR}       Testing Role