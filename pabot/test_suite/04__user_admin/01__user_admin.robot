*** Settings ***
Resource    ../keywords.robot
Suite Setup      Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}  Open Alex
Suite Teardown   Close Application
Force Tags      UserAdmin

*** Test Cases ***
#Create New User
#    [Documentation]    Admin user can navigate to User Admin and create a new user under user management
#    ...                Insert all the details required and save.
#    ...                On relogin with the new user it should return the change password page
#    [Tags]      CreateUser
#    Go To Settings      User Admin
#    Add User        example.user1
#    Log Out
#    Input Username      example.user1
#    Continue
#    Input Password      ${USER_PASSWORD}
#    Submit Credentials
#    Password Reset Page Should Open
#    Reset Password And Login
#    Log Out
#    Login Page Should Be Open
#    [Teardown]  Open Alex

Create New Role
    [Documentation]    Admin user can navigate to User Admin and create a new role under roles management
    ...                Insert all the details required and save.
    ...                On relogin with the new user it should return the change password page
    [Tags]      CreateRole
    Go To Settings      User Admin
    Add Role       test\\/
    [Teardown]  Go To Home Page

#Assign Role To User
#    [Documentation]    Admin user can navigate to User Admin and under roles management, click on Members
#    ...                Assign a user to that role
#    ...                User should be present in the list of members under that role
#    [Tags]      AssignRole
#    Go To Settings      User Admin
#    Assign Role To User     example.user1   test\\/
#    [Teardown]  Go To Home Page
#
#Check If User Is Added To Role
#    [Documentation]    New User can login
#    ...                Capture should not be visible, moreover on explore it should have just read permission
#    [Tags]      AssignRole
#    Log Out
#    Input Username      example.user1
#    Continue
#    Input Password      ${USER_PASSWORD}
#    Submit Credentials
#    Check For Rights
#    Log Out
#    Login Page Should Be Open
#    [Teardown]  Login
#
#Delete User
#    [Documentation]    Admin user can navigate to User Admin and delete a user under user management
#    [Tags]      DeleteUser
#    Go To Settings      User Admin
#    Delete User         example.user1
#    [Teardown]  Go To Home Page

Delete Role
    [Documentation]    Admin user can navigate to User Admin and under roles management, delete a role
    [Tags]      DeleteRole
    Go To Settings      User Admin
    Delete Role         test\\/
    [Teardown]   Go To Home Page

Adding User And Roles With Special Characters
    [Documentation]    Admin user can create users and roles with special charactyers in the name
    [Tags]      CreateUser  CreateRole  DeleteUser  DeleteRole
    Go To Settings      User Admin
    Add User    test\\/
    Delete User     test\\/
    Add Role    Test_Role
    Delete Role     Test_Role
    [Teardown]  Go To Home Page