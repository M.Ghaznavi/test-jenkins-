*** Settings ***
Resource    ../keywords.robot
Suite Setup      Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}  Open Alex
Suite Teardown   Close Application
Force Tags  Explore     Search

*** Test Cases ***
View Neighbours Of Single Node Selection
    [Documentation]  Alex should return results of neighbours for single node
    [Tags]      Neighbours  Fatal
    Click Explore Tab
    Search For Assets   ${NEIGHBOUR_FOR_NODE}
    Check If Neighbours Of Single Node Are Returned
    [Teardown]   Go To Home Page

View Neighbours Of Multiple Node Selection
    [Documentation]  Alex should return results of neighbours for single node
    [Tags]      Neighbours
     Click Explore Tab
     Search For Assets   ${NEIGHBOUR_FOR_NODE}
     Check If Neighbours Of Multiple Nodes Are Returned
     [Teardown]   Go To Home Page

Keep-only On Subcontext Selection
    [Documentation]  Only subcontext selection data assets remain once Keep-only is applied
    [Tags]      Keep-Only
    Click Explore Tab
    Check If Only Nodes Of Subcontext Remain
    [Teardown]   Go To Home Page


Hide On Subcontext Selection
    [Documentation]  The number of assets decreases by 2 after applying Hide to first two nodes
    [Tags]      Hide
    Click Explore Tab
    Check If The Number Of Nodes Reduces
    [Teardown]   Go To Home Page

Impact On Single Node Selection
    [Documentation]  Results of impact for the selected node should be returned
    [Tags]      Impact
    Click Explore Tab
    Search For Assets   BUS_NAME="${EXPLORE_SEARCH_NODE}"
    ${asset_type}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr/td[6][text()='${EXPLORE_SEARCH_NODE}']/parent::tr/td[5]
    Open Enrich Page for Data Object    ${EXPLORE_SEARCH_NODE}
    Expand Relationships
    ${impact_nodes}     Get Impact Nodes    ${EXPLORE_SEARCH_NODE}
    Close Enrich Page
#    @{asset_type_list}      Create List
#    ${asset_type_list}  Get Asset Type Of Nodes    ${impact_nodes}     ${asset_type_list}
#    Append To List      ${asset_type_list}  ${asset_type}
#    ${asset_type_list}      Remove Duplicates   ${asset_type_list}
#    Search For Assets   BUS_NAME="${EXPLORE_SEARCH_NODE}"
    Select Node     ${EXPLORE_SEARCH_NODE}  ${asset_type}
    Check Impact Of Data Asset
#    ${list_size}    Get Length  ${asset_type_list}
#    Expand Filter    ${TYPE_OF_ASSET}
#    :FOR  ${row}    IN RANGE    0   ${list_size}
#    \  Apply Filter     ${TYPE_OF_ASSET}    @{asset_type_list}[${row}]
    Go To Map View
    Show Hidden
    Show All
    Zoom Out Map
    Check If Impact Results For Node Is Returned    ${EXPLORE_SEARCH_NODE}  ${impact_nodes}
    [Teardown]   Go To Home Page

Impact On Multiple Node Selection
    [Documentation]  Results of impact for the selected nodes should be returned
    [Tags]      Impact
    Click Explore Tab
    ${asset_name_1}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
    ${asset_name_2}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[6]
    ${asset_type_1}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[5]
    ${asset_type_2}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[5]
    Open Enrich Page for Data Object    ${asset_name_1}
    Expand Relationships
    ${impact_nodes_1}     Get Impact Nodes    ${asset_name_1}
    Close Enrich Page
    Open Enrich Page for Data Object    ${asset_name_2}
    Expand Relationships
    ${impact_nodes_2}     Get Impact Nodes    ${asset_name_2}
    Close Enrich Page
    @{asset_type_list}      Create List
    ${asset_type_list}  Get Asset Type Of Nodes    ${impact_nodes_1}     ${asset_type_list}
    ${asset_type_list}  Get Asset Type Of Nodes    ${impact_nodes_2}     ${asset_type_list}
    Append To List      ${asset_type_list}  ${asset_type_1}
    Append To List      ${asset_type_list}  ${asset_type_2}
    ${asset_type_list}      Remove Duplicates   ${asset_type_list}
    Go To Home Page
    Click Explore Tab
    Select First Two Nodes
    Check Impact Of Data Asset
    ${list_size}    Get Length  ${asset_type_list}
    Expand Filter    ${TYPE_OF_ASSET}
    :FOR  ${row}    IN RANGE    0   ${list_size}
    \  Apply Filter     ${TYPE_OF_ASSET}    @{asset_type_list}[${row}]
    Go To Map View
    Show Hidden
    Show All
    Zoom Out Map
    Check If Impact Results For Node Is Returned    ${asset_name_1}  ${impact_nodes_1}
    Check If Impact Results For Node Is Returned    ${asset_name_2}  ${impact_nodes_2}
    [Teardown]   Go To Home Page

Lineage On Single Node Selection
    [Documentation]  Results of impact for the selected node should be returned
    [Tags]      Lineage
    Click Explore Tab
    Search For Assets   BUS_NAME="${EXPLORE_SEARCH_NODE}"
    ${asset_type}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr/td[6][text()='${EXPLORE_SEARCH_NODE}']/parent::tr/td[5]
    Open Enrich Page for Data Object    ${EXPLORE_SEARCH_NODE}
    Expand Relationships
    ${lineage_nodes}     Get Lineage Nodes    ${EXPLORE_SEARCH_NODE}
    Close Enrich Page
#    @{asset_type_list}      Create List
#    ${asset_type_list}  Get Asset Type Of Nodes    ${lineage_nodes}     ${asset_type_list}
#    Append To List      ${asset_type_list}  ${asset_type}
#    ${asset_type_list}      Remove Duplicates   ${asset_type_list}
#    Search For Assets   BUS_NAME="${EXPLORE_SEARCH_NODE}"
    Select Node     ${EXPLORE_SEARCH_NODE}  ${asset_type}
    Check Lineage Of Data Asset
#    ${list_size}    Get Length  ${asset_type_list}
#    Expand Filter    ${TYPE_OF_ASSET}
#    :FOR  ${row}    IN RANGE    0   ${list_size}
#    \  Apply Filter     ${TYPE_OF_ASSET}    @{asset_type_list}[${row}]
    Go To Map View
    Show Hidden
    Show All
    Zoom Out Map
    Check If Lineage Results For Node Is Returned    ${EXPLORE_SEARCH_NODE}  ${lineage_nodes}
    [Teardown]   Go To Home Page

Lineage On Multiple Nodes Selection
    [Documentation]  Results of impact for the selected nodes should be returned
    [Tags]      Lineage
    Click Explore Tab
    ${asset_name_1}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
    ${asset_name_2}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[6]
    ${asset_type_1}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[5]
    ${asset_type_2}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[5]
    Open Enrich Page for Data Object    ${asset_name_1}
    Expand Relationships
    ${lineage_nodes_1}     Get Lineage Nodes    ${asset_name_1}
    Close Enrich Page
    Open Enrich Page for Data Object    ${asset_name_2}
    Expand Relationships
    ${lineage_nodes_2}     Get Lineage Nodes    ${asset_name_2}
    Close Enrich Page
    @{asset_type_list}      Create List
    ${asset_type_list}  Get Asset Type Of Nodes    ${lineage_nodes_1}     ${asset_type_list}
    ${asset_type_list}  Get Asset Type Of Nodes    ${lineage_nodes_2}    ${asset_type_list}
    Append To List      ${asset_type_list}  ${asset_type_1}
    Append To List      ${asset_type_list}  ${asset_type_2}
    ${asset_type_list}      Remove Duplicates   ${asset_type_list}
    Go To Home Page
    Click Explore Tab
    Select First Two Nodes
    Check Lineage Of Data Asset
    ${list_size}    Get Length  ${asset_type_list}
    Expand Filter    ${TYPE_OF_ASSET}
    :FOR  ${row}    IN RANGE    0   ${list_size}
    \  Apply Filter     ${TYPE_OF_ASSET}    @{asset_type_list}[${row}]
    Go To Map View
    Show Hidden
    Show All
    Zoom Out Map
    Check If Lineage Results For Node Is Returned    ${asset_name_1}  ${lineage_nodes_1}
    Check If Lineage Results For Node Is Returned    ${asset_name_2}  ${lineage_nodes_2}
    [Teardown]   Go To Home Page

End-to-End On Single Node Selection
    [Documentation]  Results of impact for the selected node should be returned
    [Tags]      End-to-End
    Click Explore Tab
    Search For Assets   BUS_NAME="${EXPLORE_SEARCH_NODE}"
    ${asset_type}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr/td[6][text()='${EXPLORE_SEARCH_NODE}']/parent::tr/td[5]
    Open Enrich Page for Data Object    ${EXPLORE_SEARCH_NODE}
    Expand Relationships
    ${impact_nodes}     Get Impact Nodes    ${EXPLORE_SEARCH_NODE}
    ${lineage_nodes}     Get Lineage Nodes    ${EXPLORE_SEARCH_NODE}
    Close Enrich Page
#    @{asset_type_list}      Create List
#    ${asset_type_list}  Get Asset Type Of Nodes    ${impact_nodes}      ${asset_type_list}
#    ${asset_type_list}  Get Asset Type Of Nodes    ${lineage_nodes}     ${asset_type_list}
#    Append To List      ${asset_type_list}  ${asset_type}
#    ${asset_type_list}      Remove Duplicates   ${asset_type_list}
#    ${list_size}    Get Length  ${asset_type_list}
#    Search For Assets   BUS_NAME="${EXPLORE_SEARCH_NODE}"
    Select Node     ${EXPLORE_SEARCH_NODE}  ${asset_type}
    Check End-to-End Of Data Asset
#    Expand Filter    ${TYPE_OF_ASSET}
#    :FOR  ${row}    IN RANGE    0   ${list_size}
#    \  Apply Filter     ${TYPE_OF_ASSET}    @{asset_type_list}[${row}]
    Go To Map View
    Show Hidden
    Show All
    Zoom Out Map
    Drag And Drop By Offset      //*[@id="graph-view-svg"]    +50   -30
    Check If Impact Results For Node Is Returned    ${EXPLORE_SEARCH_NODE}  ${impact_nodes}
    Check If Lineage Results For Node Is Returned    ${EXPLORE_SEARCH_NODE}  ${lineage_nodes}
    [Teardown]   Go To Home Page

End-to-End On Multiple Nodes Selection
    [Documentation]  Results of impact for the selected node should be returned
    [Tags]      End-to-End
    Click Explore Tab
    ${asset_name_1}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
    ${asset_name_2}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[6]
    ${asset_type_1}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[5]
    ${asset_type_2}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[5]
    Open Enrich Page for Data Object    ${asset_name_1}
    Expand Relationships
    ${lineage_nodes_1}     Get Lineage Nodes    ${asset_name_1}
    ${impact_nodes_1}     Get Impact Nodes    ${asset_name_1}
    Close Enrich Page
    Open Enrich Page for Data Object    ${asset_name_2}
    Expand Relationships
    ${lineage_nodes_2}     Get Lineage Nodes    ${asset_name_2}
    ${impact_nodes_2}     Get Impact Nodes    ${asset_name_2}
    Close Enrich Page
    @{asset_type_list}      Create List
    ${asset_type_list}  Get Asset Type Of Nodes    ${lineage_nodes_1}     ${asset_type_list}
    ${asset_type_list}  Get Asset Type Of Nodes    ${lineage_nodes_2}    ${asset_type_list}
    ${asset_type_list}  Get Asset Type Of Nodes    ${impact_nodes_1}     ${asset_type_list}
    ${asset_type_list}  Get Asset Type Of Nodes    ${impact_nodes_2}     ${asset_type_list}
    Append To List      ${asset_type_list}  ${asset_type_1}
    Append To List      ${asset_type_list}  ${asset_type_2}
    ${asset_type_list}      Remove Duplicates   ${asset_type_list}
    Go To Home Page
    Click Explore Tab
    Select First Two Nodes
    Check End-to-End Of Data Asset
    ${list_size}    Get Length  ${asset_type_list}
    Expand Filter    ${TYPE_OF_ASSET}
    :FOR  ${row}    IN RANGE    0   ${list_size}
    \  Apply Filter     ${TYPE_OF_ASSET}    @{asset_type_list}[${row}]
    Go To Map View
    Show Hidden
    Show All
    Zoom Out Map
    Check If Lineage Results For Node Is Returned    ${asset_name_1}  ${lineage_nodes_1}
    Check If Lineage Results For Node Is Returned    ${asset_name_2}  ${lineage_nodes_2}
    Check If Impact Results For Node Is Returned    ${asset_name_1}  ${impact_nodes_1}
    Check If Impact Results For Node Is Returned    ${asset_name_2}  ${impact_nodes_2}
    [Teardown]   Go To Home Page

Traverse To Results On Last Page
    [Documentation]    In the explore table view, performing a search and traversing to the last page should contain results
    [Tags]      Traverse
    Click Explore Tab
    Search For Assets   ${PROPERTY_SEARCH_TERM}
    Check For Results On Last Page
    [Teardown]   Go To Home Page









