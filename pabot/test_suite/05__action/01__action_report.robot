*** Settings ***
Resource    ../keywords.robot
Suite Setup      Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}  Open Alex
Suite Teardown   Close Application
Force Tags      Action  Report

*** Test Cases ***
Create Report
    [Documentation]    User can navigate to Action and under the reporting tab create a new report
                       ...                Insert all the details required and save.
                       ...                On refresh the new report should appear in the table
    [Tags]      Create
    Click Action Tab
    Go To Reporting
    Add New Report      Upgrade_Test_Report
    Add Mandatory Details To Report
    Save Report     Upgrade_Test_Report
    Check If Report Added       Upgrade_Test_Report
    [Teardown]  Go To Home Page

#Execute Report
#    [Documentation]    User can navigate to Action and under the reporting tab select a already existing report
#                       ...                Click on the report
#                       ...                THe report should open up with the expected contents
#    [Tags]     Execute
#    Click Action Tab
#    Go To Reporting
#    Open Report          Upgrade_Test_Report
#    Check For Contents Of Report    Upgrade_Test_Report
#    [Teardown]  Go To Home Page

Delete Report
    [Documentation]    User can navigate to Action and under the reporting tab select a already existing report
                       ...                Click on the report, go to Actions and select Delete
                       ...                The report should no longer appear in the list of reports
    [Tags]     Delete
    Click Action Tab
    Go To Reporting
    Open Report          Upgrade_Test_Report
    Action Report        Delete
    Check If Report Deleted     Upgrade_Test_Report
    [Teardown]  Go To Home Page

#Create Report With Special Characters in Name
#    [Documentation]    User can navigate to Action and under the reporting tab create a new report with a \ in it's name
#                       ...                Insert all the details required and save.
#                       ...                On refresh the new report should appear in the table
#    [Tags]      Create
#    Click Action Tab
#    Go To Reporting
#    Add New Report      Upgrade_Test\\_Report
#    Add Mandatory Details To Report
#    Save Report     Upgrade_Test\\_Report
#    Check If Report Added       Upgrade_Test\\_Report
#    [Teardown]  Go To Home Page
#
#Execute Report With Special Characters in Name
#    [Documentation]    User can navigate to Action and under the reporting tab select a already existing report with a \ in the name
#                       ...                Click on the report
#                       ...                THe report should open up with the expected contents
#    [Tags]     Execute
#    Click Action Tab
#    Go To Reporting
#    Open Report          Upgrade_Test\\_Report
#    Check For Contents Of Report    Upgrade_Test\\_Report
#    [Teardown]  Go To Home Page
#
#Delete Report With Special Characters in Name
#    [Documentation]    User can navigate to Action and under the reporting tab select a already existing report with the \ in it's name
#                       ...                Click on the report, go to Actions and select Delete
#                       ...                The report should no longer appear in the list of reports
#    [Tags]     Delete
#    Click Action Tab
#    Go To Reporting
#    Open Report          Upgrade_Test\\_Report
#    Action Report        Delete
#    Check If Report Deleted     Upgrade_Test\\_Report
#    [Teardown]  Go To Home Page